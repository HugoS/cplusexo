#pragma once
#include <iostream>



class Point
{
protected:
    double _x, _y;



public:
    Point()
    {
        _x = 0; _y = 0;
    }



    Point(double x, double y)
    {
        _x = x;
        _y = y;
    }



    virtual void affiche() const
    {
        std::cout << "Point = " << _x << ", " << _y << std::endl;
    }
};
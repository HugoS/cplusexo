#pragma once
#include <iostream>
#include <string>
using namespace std;
class Personnage
{
public:
	int HitPoints;
	int arm;	
	int buckler = 3;
	int armor = 3;
	bool cancel = false;
	bool haveBuckler = false;
	bool wearArmor = false;
	void Engage(Personnage& opponent) {

		while (HitPoints > 0 && opponent.HitPoints > 0) {
			
			takeDamage(opponent);
			if (HitPoints <= 0) {
				break;
			}
			opponent.takeDamage(*this);
			if (opponent.HitPoints <= 0) {
				break;
			}


		}
		

	}
	void takeDamage(Personnage &opponent) {

		if (opponent.haveBuckler == true && opponent.buckler > 0 && opponent.cancel == false) {
			opponent.cancel = true;
			if (arm == 6 ) {
				
				opponent.buckler -= 1;
			}
			
			
		}
		if(opponent.wearArmor==true)
		{
			opponent.arm -= 1;
			arm -= opponent.armor;
			opponent.HitPoints -= arm;
		}
		else
		{
			opponent.cancel = false;
			opponent.HitPoints -= arm;
		}
	};
	void Equip(const std::string& item) {
		
		if (item == "buckler") 
		{
			haveBuckler = true;

		}
		if (item == "armor")
		{
			wearArmor = true;
		}

	};

};